﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class move : MonoBehaviour
{
    float Startx;
    // Start is called before the first frame update
    void Start()
    {
        Startx = gameObject.transform.position.x;
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.position = new Vector3(Startx + Mathf.Sin(Time.time/3)*10, gameObject.transform.position.y, gameObject.transform.position.z);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class updown : MonoBehaviour
{
    float Starty;
    float StartRo;
    // Start is called before the first frame update
    void Start()
    {
        StartRo = gameObject.transform.eulerAngles.y;
        Starty = gameObject.transform.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.position = new Vector3(gameObject.transform.position.x, Starty + Mathf.Sin(Time.time)/4, gameObject.transform.position.z);
        gameObject.transform.eulerAngles = new Vector3(gameObject.transform.rotation.x, StartRo + Mathf.Sin(Time.time/5)*15, gameObject.transform.rotation.z);
    }
}

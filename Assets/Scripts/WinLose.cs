﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Contributer: Emma Steiner
public class WinLose : MonoBehaviour
{
    public GameLogic gameLogic;

    public int maxBadRounds;
    public int maxRounds;
    public int roundfailed = 0;

    [Header("BoxColEnd")]
    public GameObject boxCol;

    //BrainSizes
    public Vector3 brain5;
    public Vector3 brain4;
    public Vector3 brain3;
    public Vector3 brain2;
    public Vector3 brain1;

    float timer;
    private float startTime;
    public float shrinkSpeed;

    public Vector3 brainMinus;

    public Vector3 currentBrainsize;

    public GameObject brain;

    //UIElements

    public GameObject gameOverNegativ;
    public GameObject gameOverPositiv;
    public GameObject mittagsPause;


    public bool Brain_ischanging;
    public Vector3 Current_brainsize;
    public float brain_timer;
    Vector3 brainsizetype;

    private void Awake()
    {
        gameOverNegativ.SetActive(false);
        gameOverPositiv.SetActive(false);
        mittagsPause.SetActive(false);

        boxCol.SetActive(false);
    }

    void Start()
    {
        brain.transform.localScale = brain5;
        Current_brainsize = brain5;

        timer = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        Current_brainsize = brain.transform.localScale;

        timer += Time.deltaTime;
        brain_timer += Time.deltaTime/2;

        //EndWhenBrainDead();
        Check_Brainsize();
        EndWhenAllWorkDone();
    }

    void Check_Brainsize()
    {
        switch (roundfailed)
        {
            case -1:
                Change_Brainsize(Vector3.zero);
                brain_timer = 0;
                break;
            case 0:
                Change_Brainsize(brain5);
                brain_timer = 0;
                break;
            case 1:
                Change_Brainsize(brain4);
                brain_timer = 0;
                break;
            case 2:
                Change_Brainsize(brain3);
                brain_timer = 0;
                break;
            case 3:
                Change_Brainsize(brain2);
                brain_timer = 0;
                break;
            case 4:
                Change_Brainsize(brain1);
                brain_timer = 0;
                break;
            default:
                Change_Brainsize(brain5);
                brain_timer = 0;
                break;

        }

        if (maxBadRounds == roundfailed)
        {
            //Game Over :(
            gameOverNegativ.SetActive(true);
            boxCol.SetActive(true);
        }

    }

    void Change_Brainsize( Vector3 newbrainsize)
    { 

            brain.transform.localScale = Vector3.Lerp(Current_brainsize,newbrainsize, brain_timer);
        
    }

    /*void EndWhenBrainDead()
    {
            //Change Brain accordingly
            if (roundfailed <= 0)
            {
                brain.transform.localScale = brain5; //smallest brain size
            }

            if (roundfailed == 1)
            {
                StartCoroutine(ScaleBrainDown());
                currentBrainsize = brain.transform.localScale;
                brain.transform.localScale = brain4;
            }
            if (roundfailed == 2)
            {
                StartCoroutine(ScaleBrainDown());
                currentBrainsize = brain.transform.localScale;
                brain.transform.localScale = brain3;
            }

            if (roundfailed == 3)
            {
                StartCoroutine(ScaleBrainDown());
                currentBrainsize = brain.transform.localScale = brain2;

            }
            if (roundfailed == 4)
            {
                StartCoroutine(ScaleBrainDown());
                currentBrainsize = brain.transform.localScale = brain1;

            }

            //End game if maxBad has been accieved

         
        
    }

    IEnumerator ScaleBrainDown()
    {
        for (float t = 0; t < 1; t += Time.deltaTime/ shrinkSpeed)
        {
            brain.transform.localScale = Vector3.Lerp(currentBrainsize, currentBrainsize - brainMinus, t);


        }
        yield return null;
    }*/
    
    void EndWhenAllWorkDone()
    {
        if (gameLogic.round == maxRounds)
        {
            //Game Over :)
            gameOverPositiv.SetActive(true);
            boxCol.SetActive(true);
        }
    }

    // UI Buttons
    public void RestartGame()
    {
        //SceneManager.GetActiveScene();
    }
    public void QuitGame()
    {
        Application.Quit();
    }
    public void ContinueGame()
    {
        mittagsPause.SetActive(false);
        boxCol.SetActive(false);
    }

    // UI Menu

    public void Mittagspause()
    {
        mittagsPause.SetActive(true);
        boxCol.SetActive(true);
    }
}
    


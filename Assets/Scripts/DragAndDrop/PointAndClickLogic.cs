﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Contributor Philipp Juhl

public class PointAndClickLogic : MonoBehaviour
{
    public static PointAndClickLogic Instance;

    [SerializeField] public GameObject StartSlot;
    [SerializeField] public GameObject TargetSlot;
    [SerializeField] public GameObject CardToMove;
    [SerializeField] public GameObject CardToSwop;
    [SerializeField] public GameObject MostLeftReserveSlot;
    [Space]
    [SerializeField] public bool hasStartSlot = false;
    [SerializeField] public bool isFromReserve = false;
    [SerializeField] public bool hasTargetSlot = false;
    [SerializeField] public bool isMovingCard = false;
    [SerializeField] public bool isSwopingCards = false;
    [SerializeField] public bool isSwopingDelayedCards = false;
    [SerializeField] public bool isExecutingDelayedActions = false;
    public bool setCardClear = false;
    //bool isReadyToGetSwappedCard = false;
    [Space]
    public float cardMoveSpeed;
    public float cardSnapSpeed;
    public float cardElevation = 0.5f;
    public float maxX = 1000;
    public float maxY = 1000;
    [Space]
    public List<GameObject> EmptyReserveSlots = new List<GameObject>();
    public List<GameObject> PlannedTransfers = new List<GameObject>();
    public List<GameObject> PlannedSwopTransfers = new List<GameObject>();
    bool soundTriggert = false;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }
    void Update()
    {
        GameLogic.Instance.Count_ForRealThisTime();
        //Get most left slot in reserve
        foreach (GameObject Slot in EmptyReserveSlots)
        {
            float x = Slot.transform.position.x;
            float y = Slot.transform.position.y;
            if (x < maxX && y < maxY)
            {
                maxX = x;
                maxY = y;
                MostLeftReserveSlot = Slot;
            }
        }

        foreach (GameObject Card in PlannedTransfers)
        {
            GameObject TargetSlot = Card.GetComponent<CardVar>().DelayedTarget;
            if(TargetSlot.GetComponent<PointAndClick>().hasPlacedCard)
            {
                TargetSlot.GetComponent<PointAndClick>().willSwap = true;
                TargetSlot.GetComponent<PointAndClick>().SlotToSwap = Card.GetComponent<CardVar>().StartSlot;
            }
        }

        if (isMovingCard)
        {
            MoveTo();
        }
        else if(isSwopingCards)
        {
            Swop();
        }
        if(isExecutingDelayedActions)
        {
            if (!soundTriggert)
            {
                AudioManager.instance.PlaySound("CardsSwitched");
                soundTriggert = true;
            }
            foreach (GameObject copiedCard in GameLogic.Instance.CopiedCards)
            {
                Destroy(copiedCard);
            }
            DelayedMoveTo();
            StartCoroutine(WaitForDelay());
        }
        if(setCardClear)
        {
            isExecutingDelayedActions = false;
        }
    }

    void MoveTo() //Move Card on free Slot
    {
        ProgrammedAnimations.Instance.Allow_Connections = false;
        CardToMove.transform.position = Vector3.Lerp(CardToMove.transform.position, TargetSlot.transform.position, cardMoveSpeed * Time.deltaTime); //Lerp Card towards TargetSlot
        if (Vector3.Distance(CardToMove.transform.position, TargetSlot.transform.position) < 0.1f) //once targetslot is reached
        {
            SetClear();
        }
    }

    void Swop() //Swops Cards between 2 Slots
    {
        ProgrammedAnimations.Instance.Allow_Connections = false;
        CardToMove.transform.position = Vector3.Lerp(CardToMove.transform.position, TargetSlot.transform.position, cardMoveSpeed * Time.deltaTime); //Lerp CardToMove towards TargetSlot
        CardToSwop.transform.position = Vector3.Lerp(CardToSwop.transform.position, StartSlot.transform.position, cardMoveSpeed * Time.deltaTime);  //Lerp CardToSwop towards StartSlot
        if (Vector3.Distance(CardToMove.transform.position, TargetSlot.transform.position) <= 0.1f || Vector3.Distance(CardToSwop.transform.position, StartSlot.transform.position) <= 0.1f)
        {
            SetClear();
        }
    }

    void DelayedMoveTo()
    {
        //isMovingCard = true;
        foreach (GameObject CardToTransfer in PlannedTransfers)
        {  
            Vector3 delayedTargetPos = CardToTransfer.GetComponent<CardVar>().DelayedTarget.transform.position;
            CardToTransfer.transform.position = Vector3.Lerp(CardToTransfer.transform.position, delayedTargetPos, cardMoveSpeed * Time.deltaTime);
            if(CardToTransfer.GetComponent<CardVar>().cardIsFromReserve && GameLogic.Instance.InInventory.Contains(CardToTransfer))
            {
                GameLogic.Instance.InInventory.Remove(CardToTransfer); //Remove from InventoryList when on Mainboard
            }
            else if(!CardToTransfer.GetComponent<CardVar>().cardIsFromReserve && !GameLogic.Instance.InInventory.Contains(CardToTransfer))
            {
                GameLogic.Instance.InInventory.Add(CardToTransfer); //Add to InventoryList when in Reserve
            }

            if (CardToTransfer.GetComponent<CardVar>().isRemovedFromSlot == false)
            {
                //StartSlot ejects transferred Card
                GameObject StartSlot = CardToTransfer.GetComponent<CardVar>().StartSlot;
                StartSlot.GetComponent<PointAndClick>().PlacedCard.transform.parent = null;
                StartSlot.GetComponent<PointAndClick>().PlacedCard = GameObject.Find("SlotPlaceHolder");
                StartSlot.GetComponent<PointAndClick>().hasPlacedCard = false;
                CardToTransfer.GetComponent<CardVar>().isRemovedFromSlot = true;
                StartSlot.GetComponent<PointAndClick>().willSwap = false;
                SetClear();
            }

            //TargetSlot gets transferred Card
            GameObject TargetSlotForDelay = CardToTransfer.GetComponent<CardVar>().DelayedTarget;
            TargetSlotForDelay.GetComponent<PointAndClick>().hasPlacedCard = true;
            TargetSlotForDelay.GetComponent<PointAndClick>().PlacedCard = CardToTransfer;
            CardToTransfer.transform.parent = TargetSlotForDelay.transform;
            TargetSlotForDelay.GetComponent<PointAndClick>().willSwap = false;
            TargetSlotForDelay.GetComponent<PointAndClick>().hasCardPreshow = false;
            TargetSlotForDelay.GetComponent<PointAndClick>().isTargetOfDelayedMove = false;

            GameLogic.Instance.Count_ForRealThisTime();

            if (setCardClear)
            {
                CardToTransfer.GetComponent<CardVar>().DelayedTarget = null;
                CardToTransfer.GetComponent<CardVar>().StartSlot = null;
                CardToTransfer.GetComponent<CardVar>().cardIsFromReserve = false;
                CardToTransfer.GetComponent<CardVar>().isDelayedSwopped = false;
                CardToTransfer.GetComponent<CardVar>().isRemovedFromSlot = false;
                CardToTransfer.GetComponent<CardVar>().SetCardClear();

                StartSlot.GetComponent<PointAndClick>().willSwap = false;
                TargetSlotForDelay.GetComponent<PointAndClick>().willSwap = false;
                TargetSlotForDelay.GetComponent<PointAndClick>().hasCardPreshow = false;
                TargetSlotForDelay.GetComponent<PointAndClick>().isTargetOfDelayedMove = false;
                TargetSlotForDelay.GetComponent<PointAndClick>().hasPlacedCard = false;
                PlannedTransfers.Remove(CardToTransfer);
                isExecutingDelayedActions = false;
            }
        }
    }

    IEnumerator WaitForDelay()
    {
        yield return new WaitForSeconds(0.9f);
        setCardClear = true;
        yield return new WaitForSeconds(1);
        isExecutingDelayedActions = false;
        setCardClear = false;
        isMovingCard = false;
        PlannedTransfers.Clear();
        PlannedSwopTransfers.Clear();
        GameLogic.Instance.CopiedCards.Clear();
        soundTriggert = false;
    }

    public void SetClear()
    {
        ProgrammedAnimations.Instance.Allow_Connections = true;
        GameLogic.Instance.ArtArsenal.Deactivate_Cardholder(); //Deactivates all cardholder shaders
        StartSlot.GetComponent<PointAndClick>().canReceiveSwappedCard = false;
        StartSlot.GetComponent<PointAndClick>().isElevatingCard = false;
        hasStartSlot = false;
        isFromReserve = false;
        hasTargetSlot = false;
        isMovingCard = false;
        isSwopingCards = false;
        StartSlot = GameObject.Find("PlaceHolder");
        TargetSlot = GameObject.Find("PlaceHolder (1)");
        CardToMove = GameObject.Find("PlaceHolder (2)");
        CardToSwop = GameObject.Find("PlaceHolder (3)");
        return;
    }
}
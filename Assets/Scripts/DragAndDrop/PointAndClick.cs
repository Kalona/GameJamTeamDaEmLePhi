﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Contributor Philipp Juhl

public class PointAndClick : MonoBehaviour
{
    [SerializeField] public GameObject PlacedCard;
    [SerializeField] public GameObject SlotToSwap;
    [SerializeField] public GameObject CardPreshow;
    [Space]
    [SerializeField] public bool hasPlacedCard = false;
    [SerializeField] public bool isAdjacentToTargetSlot = false;
    [SerializeField] public bool canReceiveSwappedCard = false;
    [SerializeField] bool willReceiveSwap = false;
    [SerializeField] public bool willSwap = false;
    [SerializeField] public bool hasCardPreshow = false;
    public bool hasChosenCard;
    public bool isTargetOfDelayedMove = false;
    public bool isStartOfDelayedMove = false;
    public bool isElevatingCard = false;
    public bool removeFromTransfers = false;
    [SerializeField] bool isSwepped = false;

    public enum TYPE { BoardTile, Reserve, ShopOffer }
    [SerializeField] TYPE Type = TYPE.BoardTile;

    [SerializeField] List<GameObject> adjacentTiles = new List<GameObject>();

    Vector3 centerPosition;
    Vector3 elevatedPosition;
    [SerializeField] GameObject ShiftedPosition;
    [SerializeField] GameObject sweppedPostion;
    [SerializeField] GameObject ShiftedStart;

    void Start()
    {
        PlacedCard = GameObject.Find("SlotPlaceHolder");
        
    }

    void Update()
    {
        centerPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z); //Define center position of Slot
        elevatedPosition = transform.position + new Vector3(0, 0, -PointAndClickLogic.Instance.cardElevation); //Define elevated position of Slot

        if (PlacedCard == null)
        {
            PlacedCard = GameObject.Find("SlotPlaceHolder"); //small fix so that Console won't show missing references as errors when destroying rewardcards
        }
        if(SlotToSwap == null)
        {
            SlotToSwap = GameObject.Find("SlotToSwapPlaceHolder");
        }

        switch (Type)
        {
            case TYPE.BoardTile:

                break;
            case TYPE.Reserve:

                break;
            case TYPE.ShopOffer:

                break;
        }

        //elevatedPosition = transform.position + new Vector3(0, 0, 1);
        if (Type == TYPE.BoardTile || Type == TYPE.Reserve)
        {
            if (isElevatingCard)
            {
                PlacedCard.transform.position = Vector3.Lerp(PlacedCard.transform.position, elevatedPosition, PointAndClickLogic.Instance.cardMoveSpeed * Time.deltaTime);
            }
            if (hasCardPreshow)
            {
                CardPreshow.transform.position = Vector3.Lerp(CardPreshow.transform.position, ShiftedPosition.transform.position, PointAndClickLogic.Instance.cardMoveSpeed * Time.deltaTime);
            }
            if (isSwepped)
            {
                ShiftedPosition.transform.position = Vector3.Lerp(ShiftedPosition.transform.position, sweppedPostion.transform.position, PointAndClickLogic.Instance.cardMoveSpeed * Time.deltaTime);
            }
            else if(hasCardPreshow && !isSwepped)
            {
                ShiftedPosition.transform.position = Vector3.Lerp(ShiftedPosition.transform.position, ShiftedStart.transform.position, PointAndClickLogic.Instance.cardMoveSpeed * Time.deltaTime);
            }
            if(!hasCardPreshow)
            {
                Destroy(CardPreshow);
            }
        }

        if(PointAndClickLogic.Instance.isExecutingDelayedActions)
        {
            hasCardPreshow = false;
        }

        //Empty Reserve Slot?
        if (Type == TYPE.Reserve && !hasPlacedCard && !PointAndClickLogic.Instance.EmptyReserveSlots.Contains(gameObject))
        {
            PointAndClickLogic.Instance.EmptyReserveSlots.Add(gameObject);
        }
        else if(Type == TYPE.Reserve && hasPlacedCard && PointAndClickLogic.Instance.EmptyReserveSlots.Contains(gameObject))
        {
            PointAndClickLogic.Instance.EmptyReserveSlots.Remove(gameObject);
            PointAndClickLogic.Instance.maxX = 1000;
            PointAndClickLogic.Instance.maxY = 1000;
        }

        if (!PointAndClickLogic.Instance.isMovingCard || !PointAndClickLogic.Instance.isSwopingCards)
        {
            isAdjacentToTargetSlot = false;
        }

        //Receive Swapped Card
        if (canReceiveSwappedCard && willReceiveSwap)
        {
            hasPlacedCard = true;
            PlacedCard = PointAndClickLogic.Instance.CardToSwop;
            canReceiveSwappedCard = false;
            willReceiveSwap = false;
        }
        //Currently Swapping?
        if (PointAndClickLogic.Instance.isSwopingCards)
        {
            willReceiveSwap = true;
        }
        else if (!PointAndClickLogic.Instance.isSwopingCards)
        {
            willReceiveSwap = false;
        }
        
        if (PointAndClickLogic.Instance.hasStartSlot == false)
        {
            canReceiveSwappedCard = false;
        }

        if (hasPlacedCard && !PointAndClickLogic.Instance.isSwopingCards && !isElevatingCard)
        {
            MoveCardToCenter();
            PlacedCard.transform.parent = gameObject.transform;
        }
        else if (!hasPlacedCard)
        {
            PlacedCard.transform.parent = null;
            PlacedCard = GameObject.Find("SlotPlaceHolder");
        }

        if(hasPlacedCard)
        {
            RotateCardAlongSlot();
            //PlacedCard.transform.parent = gameObject.transform;
        }

        if(!SlotToSwap.GetComponent<PointAndClick>().isStartOfDelayedMove)
        {
            isTargetOfDelayedMove = false;
        }

        //Establish transfer of placed card when delayed card will move to this slot
        if(hasPlacedCard && isTargetOfDelayedMove && !PointAndClickLogic.Instance.PlannedTransfers.Contains(PlacedCard))
        {
            PointAndClickLogic.Instance.PlannedTransfers.Add(PlacedCard);
            PlacedCard.GetComponent<CardVar>().DelayedTarget = SlotToSwap;
            PlacedCard.GetComponent<CardVar>().StartSlot = gameObject;
        }
        else if(PointAndClickLogic.Instance.PlannedTransfers.Contains(PlacedCard) && !PointAndClickLogic.Instance.isExecutingDelayedActions && !hasPlacedCard)
        {
            PointAndClickLogic.Instance.PlannedTransfers.Remove(PlacedCard);
            PlacedCard.GetComponent<CardVar>().DelayedTarget = null;
            PlacedCard.GetComponent<CardVar>().StartSlot.GetComponent<PointAndClick>().willReceiveSwap = false;
            PlacedCard.GetComponent<CardVar>().StartSlot = null;
            PlacedCard.GetComponent<CardVar>().SetCardClear();
            willSwap = false;
            isTargetOfDelayedMove = false;
            PointAndClickLogic.Instance.SetClear();
        }

        //Receive chosen card from offer
        if(Type == TYPE.Reserve && CardOffer.Instance.emptyReserveTile == gameObject && CardOffer.Instance.hasSelectedCard == true && hasChosenCard == false)
        {
            hasPlacedCard = true;
            PlacedCard = CardOffer.Instance.selectedCard;
            PlacedCard.transform.parent = gameObject.transform;
            hasChosenCard = true;
        }
    }

    public void RemoveFromTransfers()
    {
        if(PointAndClickLogic.Instance.PlannedTransfers.Contains(PlacedCard) && !PointAndClickLogic.Instance.isExecutingDelayedActions && !PointAndClickLogic.Instance.isSwopingCards)
        {
            PointAndClickLogic.Instance.PlannedTransfers.Remove(PlacedCard);
            //PlacedCard.GetComponent<CardVar>().DelayedTarget = null;
            PlacedCard.GetComponent<CardVar>().StartSlot.GetComponent<PointAndClick>().willReceiveSwap = false;
            //PlacedCard.GetComponent<CardVar>().StartSlot = null;
            PlacedCard.GetComponent<CardVar>().SetCardClear();
            willSwap = false;
            isTargetOfDelayedMove = false;
            //PointAndClickLogic.Instance.SetClear();
        }
    }

    void OnMouseExit()
    {
        isSwepped = false;
    }

    void OnMouseOver()
    {
        if (hasCardPreshow && hasPlacedCard)
        {
            isSwepped = true;
        }

        //If mouse hovers over the GameObject        Feedback triggern?
        if (Input.GetMouseButtonDown(0) && !PointAndClickLogic.Instance.isMovingCard && !PointAndClickLogic.Instance.isSwopingCards && !PointAndClickLogic.Instance.isExecutingDelayedActions)
        {
            if (Type == TYPE.ShopOffer && CardOffer.Instance.offerAvailable && PointAndClickLogic.Instance.EmptyReserveSlots.Count > 0)
            {
                CardOffer.Instance.emptyReserveTile = PointAndClickLogic.Instance.MostLeftReserveSlot;
                CardOffer.Instance.selectedCard = PlacedCard;
                CardOffer.Instance.hasSelectedCard = true;
                PlacedCard.transform.parent = null;
                PlacedCard = GameObject.Find("SlotPlaceHolder");
                hasPlacedCard = false;
                PlacedCard.GetComponent<CardVar>().cardIsFromReserve = false;
            }

            if (Type == TYPE.BoardTile && GameLogic.Instance.remainingActions > 0 && !CardOffer.Instance.offerAvailable)
            {
                if (!PointAndClickLogic.Instance.hasStartSlot && hasPlacedCard) //No StartSlot yet => set this as StartSlot
                {
                    if(!isStartOfDelayedMove)
                    {
                        GameLogic.Instance.ArtArsenal.Activate_Cardholder(); //Dala's stuff to detect on which cardholder you are on

                        PointAndClickLogic.Instance.hasStartSlot = true;
                        PointAndClickLogic.Instance.StartSlot = gameObject;
                        PointAndClickLogic.Instance.CardToMove = PlacedCard;
                        //PlacedCard = GameObject.Find("SlotPlaceHolder");
                        //hasPlacedCard = false;
                        canReceiveSwappedCard = true;
                        PlacedCard.GetComponent<CardVar>().StartSlot = gameObject;
                        PlacedCard.GetComponent<CardVar>().isRemovedFromSlot = false;
                        PlacedCard.GetComponent<CardVar>().isDelayedSwopped = false;
                        isElevatingCard = true;
                        AudioManager.instance.PlaySound("CardPickedUp");
                    }
                    else if(isStartOfDelayedMove)
                    {
                        PointAndClickLogic.Instance.PlannedTransfers.Remove(PointAndClickLogic.Instance.CardToMove);
                        PlacedCard.GetComponent<CardVar>().DelayedTarget.GetComponent<PointAndClick>().isTargetOfDelayedMove = false;
                        PlacedCard.GetComponent<CardVar>().DelayedTarget.GetComponent<PointAndClick>().willSwap = false;
                        PointAndClickLogic.Instance.PlannedTransfers.Remove(PlacedCard.GetComponent<CardVar>().DelayedTarget.GetComponent<PointAndClick>().PlacedCard);
                        PlacedCard.GetComponent<CardVar>().SetCardClear();
                        isStartOfDelayedMove = false;
                        isTargetOfDelayedMove = false;
                        //PointAndClickLogic.Instance.SetClear();

                        GameLogic.Instance.ArtArsenal.Activate_Cardholder(); //Dala's stuff to detect on which cardholder you are on

                        PointAndClickLogic.Instance.hasStartSlot = true; //Ich muss fucking Funktionen dafür schreiben
                        PointAndClickLogic.Instance.StartSlot = gameObject;
                        PointAndClickLogic.Instance.CardToMove = PlacedCard;
                        PointAndClickLogic.Instance.PlannedTransfers.Remove(PlacedCard);
                        //PlacedCard = GameObject.Find("SlotPlaceHolder");
                        //hasPlacedCard = false;
                        canReceiveSwappedCard = true;
                        PlacedCard.GetComponent<CardVar>().StartSlot = gameObject;
                        PlacedCard.GetComponent<CardVar>().isRemovedFromSlot = false;
                        PlacedCard.GetComponent<CardVar>().isDelayedSwopped = false;
                        isElevatingCard = true;
                        AudioManager.instance.PlaySound("CardPickedUp");
                    }
                }

                else if (PointAndClickLogic.Instance.hasStartSlot && adjacentTiles.Contains(PointAndClickLogic.Instance.StartSlot)) //Has StartSlot adjacent to this => set this as TargetSlot
                {
                    GameLogic.Instance.ArtArsenal.Activate_Cardholder(); //Dala's stuff to detect on which cardholder you are on
                    GameLogic.Instance.remainingActions--;
                    AudioManager.instance.PlaySound("CardLayedDown");

                    if (!hasPlacedCard) //no Card on TargetSlot => move to TargetSlot
                    {
                        Debug.Log("Target");
                        PointAndClickLogic.Instance.StartSlot.GetComponent<PointAndClick>().RemoveFromTransfers();
                        PointAndClickLogic.Instance.StartSlot.GetComponent<PointAndClick>().PlacedCard = GameObject.Find("SlotPlaceHolder");
                        PointAndClickLogic.Instance.StartSlot.GetComponent<PointAndClick>().PlacedCard.transform.parent = null;
                        PointAndClickLogic.Instance.StartSlot.GetComponent<PointAndClick>().hasPlacedCard = false;

                        PointAndClickLogic.Instance.hasTargetSlot = true;
                        PointAndClickLogic.Instance.TargetSlot = gameObject;
                        PointAndClickLogic.Instance.isMovingCard = true;
                        GetMovedCard();
                    }
                    else if (hasPlacedCard) //has Card on TargetSlot => swop with TargetSlot
                    {
                        if(!PointAndClickLogic.Instance.StartSlot.GetComponent<PointAndClick>().isTargetOfDelayedMove)
                        {
                            PointAndClickLogic.Instance.StartSlot.GetComponent<PointAndClick>().RemoveFromTransfers();
                            PointAndClickLogic.Instance.StartSlot.GetComponent<PointAndClick>().PlacedCard = GameObject.Find("SlotPlaceHolder");
                            PointAndClickLogic.Instance.StartSlot.GetComponent<PointAndClick>().hasPlacedCard = false;
                        }
                        if (isStartOfDelayedMove)
                        {
                            PointAndClickLogic.Instance.PlannedTransfers.Remove(PlacedCard);
                            PlacedCard.GetComponent<CardVar>().DelayedTarget.GetComponent<PointAndClick>().isTargetOfDelayedMove = false;
                            PlacedCard.GetComponent<CardVar>().DelayedTarget.GetComponent<PointAndClick>().willSwap = false;
                            PointAndClickLogic.Instance.PlannedTransfers.Remove(PlacedCard.GetComponent<CardVar>().DelayedTarget.GetComponent<PointAndClick>().PlacedCard);
                            PlacedCard.GetComponent<CardVar>().SetCardClear();
                            isStartOfDelayedMove = false;
                        }

                        PointAndClickLogic.Instance.hasTargetSlot = true;
                        PointAndClickLogic.Instance.TargetSlot = gameObject;
                        PointAndClickLogic.Instance.CardToSwop = PlacedCard;
                        //PlacedCard = GameObject.Find("SlotPlaceHolder");
                        //hasPlacedCard = false;
                        AudioManager.instance.PlaySound("CardsSwitched");
                        
                        PointAndClickLogic.Instance.isMovingCard = true;
                        PointAndClickLogic.Instance.isSwopingCards = true;

                        if (PointAndClickLogic.Instance.StartSlot.GetComponent<PointAndClick>().isTargetOfDelayedMove)
                        {
                            PointAndClickLogic.Instance.StartSlot.GetComponent<PointAndClick>().RemoveFromTransfers();
                            PointAndClickLogic.Instance.StartSlot.GetComponent<PointAndClick>().PlacedCard = GameObject.Find("SlotPlaceHolder");
                            PointAndClickLogic.Instance.StartSlot.GetComponent<PointAndClick>().hasPlacedCard = false;

                            PointAndClickLogic.Instance.StartSlot.GetComponent<PointAndClick>().hasPlacedCard = true;
                            PointAndClickLogic.Instance.StartSlot.GetComponent<PointAndClick>().PlacedCard = PointAndClickLogic.Instance.CardToSwop;
                            PointAndClickLogic.Instance.StartSlot.GetComponent<PointAndClick>().canReceiveSwappedCard = false;
                            PointAndClickLogic.Instance.StartSlot.GetComponent<PointAndClick>().willReceiveSwap = false;
                        }

                        GetMovedCard();
                    }
                }
                else if (PointAndClickLogic.Instance.hasStartSlot && PointAndClickLogic.Instance.isFromReserve && !isTargetOfDelayedMove) //From Reserve to Board
                {
                    //Reset StartSlot
                    //PointAndClickLogic.Instance.StartSlot.GetComponent<PointAndClick>().PlacedCard = PointAndClickLogic.Instance.CardToMove;
                    //hasPlacedCard = true;

                    GameLogic.Instance.remainingActions--;

                    if (hasPlacedCard)
                    {
                        willSwap = true;
                        SlotToSwap = PointAndClickLogic.Instance.StartSlot;
                    }

                    isTargetOfDelayedMove = true;
                    PointAndClickLogic.Instance.CardToMove.GetComponent<CardVar>().cardIsFromReserve = true;
                    PointAndClickLogic.Instance.CardToMove.GetComponent<CardVar>().StartSlot = PointAndClickLogic.Instance.StartSlot;
                    PointAndClickLogic.Instance.StartSlot.GetComponent<PointAndClick>().isStartOfDelayedMove = true;
                    PointAndClickLogic.Instance.PlannedTransfers.Add(PointAndClickLogic.Instance.CardToMove);
                    PointAndClickLogic.Instance.CardToMove.GetComponent<CardVar>().DelayedTarget = gameObject;
                    PointAndClickLogic.Instance.CardToMove.GetComponent<CardVar>().isDelayedSwopped = true;
                    GameLogic.Instance.CopyCard(PointAndClickLogic.Instance.CardToMove, PointAndClickLogic.Instance.CardToMove.transform.position, gameObject); //Generate Preshow of Card (he doesn`t like this)
                    PointAndClickLogic.Instance.SetClear();
                }
            }

            if (Type == TYPE.Reserve)
            {
                if (!PointAndClickLogic.Instance.hasStartSlot && hasPlacedCard)
                {
                    if(!isStartOfDelayedMove)
                    {                
                        GameLogic.Instance.ArtArsenal.Activate_Cardholder(); //Dala's stuff to detect on which cardholder you are on

                        PointAndClickLogic.Instance.hasStartSlot = true;
                        PointAndClickLogic.Instance.isFromReserve = true;
                        PointAndClickLogic.Instance.StartSlot = gameObject;
                        PointAndClickLogic.Instance.CardToMove = PlacedCard;
                        canReceiveSwappedCard = true;
                        PlacedCard.GetComponent<CardVar>().StartSlot = gameObject;
                        PlacedCard.GetComponent<CardVar>().isRemovedFromSlot = false;
                        PlacedCard.GetComponent<CardVar>().isDelayedSwopped = false;
                        PlacedCard.GetComponent<CardVar>().cardIsFromReserve = true;
                        isElevatingCard = true;
                        AudioManager.instance.PlaySound("CardPickedUp");
                    }
                    else if (isStartOfDelayedMove)
                    {
                        PointAndClickLogic.Instance.PlannedTransfers.Remove(PlacedCard);
                        PlacedCard.GetComponent<CardVar>().DelayedTarget.GetComponent<PointAndClick>().isTargetOfDelayedMove = false;
                        PlacedCard.GetComponent<CardVar>().DelayedTarget.GetComponent<PointAndClick>().willSwap = false;
                        PointAndClickLogic.Instance.PlannedTransfers.Remove(PlacedCard.GetComponent<CardVar>().DelayedTarget.GetComponent<PointAndClick>().PlacedCard);
                        PlacedCard.GetComponent<CardVar>().SetCardClear();
                        isStartOfDelayedMove = false;
                        isTargetOfDelayedMove = false;
                        //PointAndClickLogic.Instance.SetClear();

                        GameLogic.Instance.ArtArsenal.Activate_Cardholder(); //Dala's stuff to detect on which cardholder you are on

                        PointAndClickLogic.Instance.hasStartSlot = true;
                        PointAndClickLogic.Instance.isFromReserve = true;
                        PointAndClickLogic.Instance.StartSlot = gameObject;
                        PointAndClickLogic.Instance.CardToMove = PlacedCard;
                        canReceiveSwappedCard = true;
                        PlacedCard.GetComponent<CardVar>().StartSlot = gameObject;
                        PlacedCard.GetComponent<CardVar>().isRemovedFromSlot = false;
                        PlacedCard.GetComponent<CardVar>().isDelayedSwopped = false;
                        PlacedCard.GetComponent<CardVar>().cardIsFromReserve = true;
                        isElevatingCard = true;
                        AudioManager.instance.PlaySound("CardPickedUp");
                    }
                }

                else if (PointAndClickLogic.Instance.hasStartSlot && PointAndClickLogic.Instance.isFromReserve) // Reserve to Reserve
                {
                    GameLogic.Instance.ArtArsenal.Activate_Cardholder(); //Dala's stuff to detect on which cardholder you are on

                    PointAndClickLogic.Instance.StartSlot.GetComponent<PointAndClick>().RemoveFromTransfers();
                    PointAndClickLogic.Instance.StartSlot.GetComponent<PointAndClick>().PlacedCard = GameObject.Find("SlotPlaceHolder");
                    PointAndClickLogic.Instance.StartSlot.GetComponent<PointAndClick>().PlacedCard.transform.parent = null;
                    PointAndClickLogic.Instance.StartSlot.GetComponent<PointAndClick>().hasPlacedCard = false;
                    AudioManager.instance.PlaySound("CardLayedDown");

                    if (!hasPlacedCard) //no Card on TargetSlot => move to TargetSlot
                    {
                        if (GameLogic.Instance.InInventory.Contains(PointAndClickLogic.Instance.CardToMove) == false)
                        {
                            GameLogic.Instance.InInventory.Add(PointAndClickLogic.Instance.CardToMove); //Dala's stuff to detect which cards are in the Inventory
                        }
                        PointAndClickLogic.Instance.hasTargetSlot = true;
                        PointAndClickLogic.Instance.TargetSlot = gameObject;
                        PointAndClickLogic.Instance.isMovingCard = true;
                        GetMovedCard();
                    }
                    else if (hasPlacedCard) //has Card on TargetSlot => swop with StartSlot
                    {
                        if (isStartOfDelayedMove)
                        {
                            PointAndClickLogic.Instance.PlannedTransfers.Remove(PlacedCard);
                            PlacedCard.GetComponent<CardVar>().DelayedTarget.GetComponent<PointAndClick>().isTargetOfDelayedMove = false;
                            PlacedCard.GetComponent<CardVar>().DelayedTarget.GetComponent<PointAndClick>().willSwap = false;
                            PointAndClickLogic.Instance.PlannedTransfers.Remove(PlacedCard.GetComponent<CardVar>().DelayedTarget.GetComponent<PointAndClick>().PlacedCard);
                            PlacedCard.GetComponent<CardVar>().SetCardClear();
                            isStartOfDelayedMove = false;
                        }

                        PointAndClickLogic.Instance.StartSlot.GetComponent<PointAndClick>().willReceiveSwap = true;
                        PointAndClickLogic.Instance.hasTargetSlot = true;
                        PointAndClickLogic.Instance.TargetSlot = gameObject;
                        PointAndClickLogic.Instance.CardToSwop = PlacedCard;
                        if (GameLogic.Instance.InInventory.Contains(PointAndClickLogic.Instance.CardToSwop) == false)
                        {
                            GameLogic.Instance.InInventory.Add(PointAndClickLogic.Instance.CardToSwop); //Places swaped Card in Inventory
                        }
                        PointAndClickLogic.Instance.isSwopingCards = true;
                        PlacedCard.transform.parent = null;
                        PlacedCard = null;
                        hasPlacedCard = false;
                        if(PointAndClickLogic.Instance.StartSlot.GetComponent<PointAndClick>().isTargetOfDelayedMove)
                        {
                            PointAndClickLogic.Instance.StartSlot.GetComponent<PointAndClick>().hasPlacedCard = true;
                            PointAndClickLogic.Instance.StartSlot.GetComponent<PointAndClick>().PlacedCard = PointAndClickLogic.Instance.CardToSwop;
                            PointAndClickLogic.Instance.StartSlot.GetComponent<PointAndClick>().canReceiveSwappedCard = false;
                            PointAndClickLogic.Instance.StartSlot.GetComponent<PointAndClick>().willReceiveSwap = false;
                        }
                        GetMovedCard();
                        AudioManager.instance.PlaySound("CardsSwitched");
                    }
                }

                else if (PointAndClickLogic.Instance.hasStartSlot && !PointAndClickLogic.Instance.isFromReserve && !isTargetOfDelayedMove && !CardOffer.Instance.offerAvailable) //From Board to Reserve
                {
                    GameLogic.Instance.remainingActions--;
                    
                    if (hasPlacedCard)
                    {
                        willSwap = true;
                        SlotToSwap = PointAndClickLogic.Instance.StartSlot;
                    }
                    isTargetOfDelayedMove = true;
                    //PointAndClickLogic.Instance.CardToMove.GetComponent<CardVar>().cardIsFromReserve = true;
                    PointAndClickLogic.Instance.CardToMove.GetComponent<CardVar>().StartSlot = PointAndClickLogic.Instance.StartSlot;
                    PointAndClickLogic.Instance.StartSlot.GetComponent<PointAndClick>().isStartOfDelayedMove = true;
                    PointAndClickLogic.Instance.PlannedTransfers.Add(PointAndClickLogic.Instance.CardToMove);
                    PointAndClickLogic.Instance.CardToMove.GetComponent<CardVar>().DelayedTarget = gameObject;
                    PointAndClickLogic.Instance.CardToMove.GetComponent<CardVar>().isDelayedSwopped = true;
                    GameLogic.Instance.CopyCard(PointAndClickLogic.Instance.CardToMove, PointAndClickLogic.Instance.CardToMove.transform.position, gameObject); //Generate Preshow of Card
                    PointAndClickLogic.Instance.SetClear();
                }
            }
        }
    }

    void GetMovedCard()
    {
        if (PointAndClickLogic.Instance.TargetSlot = gameObject)
        {
            hasPlacedCard = true;
            PlacedCard = PointAndClickLogic.Instance.CardToMove;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Card" && !PointAndClickLogic.Instance.isMovingCard && !PointAndClickLogic.Instance.isSwopingCards && !CardOffer.Instance.hasSelectedCard)
        {
            if(Type == TYPE.BoardTile && !PointAndClickLogic.Instance.isExecutingDelayedActions || Type == TYPE.ShopOffer)
            {
                PlacedCard = other.gameObject;
                hasPlacedCard = true;
            }
        }
        if(other.gameObject.tag == "Card" && Type == TYPE.ShopOffer && !CardOffer.Instance.hasSelectedCard)
        {
            PlacedCard = other.gameObject;
            hasPlacedCard = true;
        }
    }

    void MoveCardToCenter()
    {
        PlacedCard.transform.position = Vector3.Lerp(PlacedCard.transform.position, centerPosition, PointAndClickLogic.Instance.cardSnapSpeed * Time.deltaTime);
    }

    void RotateCardAlongSlot()
    {
        PlacedCard.transform.rotation = transform.rotation;
    }
}

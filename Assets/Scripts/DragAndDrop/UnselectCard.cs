﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnselectCard : MonoBehaviour
{
    private void Update()
    {
        if(Input.GetMouseButtonDown(1))
        {
            PointAndClickLogic.Instance.SetClear();
        }
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            PointAndClickLogic.Instance.SetClear();
        }
    }

    void OnMouseOver()
    {
        if(Input.GetMouseButtonDown(0))
        {
            PointAndClickLogic.Instance.SetClear();
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Contributor Philipp Juhl

public class CardOffer : MonoBehaviour
{
    public static CardOffer Instance;

    public GameObject firstOfferTile;
    public GameObject secondOfferTile;
    public GameObject thirdOfferTile;

    public GameObject selectedCard;
    public GameObject emptyReserveTile;

    public List<GameObject> OfferedCards = new List<GameObject>();

    public Vector3 nextFreeSlot;

    public bool offerAvailable;
    public bool hasSelectedCard;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    void Update()
    {
        if(hasSelectedCard)
        {
            SendSelectedCardToReserve();
            DestroyRemainingCards();
            ProgrammedAnimations.Instance.spin_side = 90; //turn back
            ProgrammedAnimations.Instance.dreh_richtung = 1;
            ProgrammedAnimations.Instance.Start_SpinRewardBoard();
        }
    }

    public void GenerateOffer(int Rank)
    {
        if (Rank == 0) //I had to put this in here, so it won't count several times in update,sorry Philipp
        {
            GameLogic.Instance.Lives_Left -= 1;
        }
        if(Rank == 3)
        {
            GameLogic.Instance.Lives_Left += 1;
        }
        offerAvailable = true;
        GameLogic.Instance.CreateCard(Rank, firstOfferTile.transform.position,true);
        GameLogic.Instance.CreateCard(Rank, secondOfferTile.transform.position,true);
        GameLogic.Instance.CreateCard(Rank, thirdOfferTile.transform.position,true);
        ProgrammedAnimations.Instance.spin_side = -90;
        ProgrammedAnimations.Instance.dreh_richtung = -1;
        GameLogic.Instance.Actionsleft.gameObject.SetActive(false); //text won't show or it will shine through
        ProgrammedAnimations.Instance.Start_SpinRewardBoard();
    }

    public void SendSelectedCardToReserve()
    {
        selectedCard.transform.position = Vector3.Lerp(selectedCard.transform.position, emptyReserveTile.transform.position, PointAndClickLogic.Instance.cardMoveSpeed * Time.deltaTime);

        emptyReserveTile.GetComponent<PointAndClick>().hasPlacedCard = true;
        emptyReserveTile.GetComponent<PointAndClick>().PlacedCard = selectedCard;
        //emptyReserveTile.GetComponent<PointAndClick>().PlacedCard.transform.parent = gameObject.transform;
        emptyReserveTile.GetComponent<PointAndClick>().hasChosenCard = true;

        if (Vector3.Distance(selectedCard.transform.position, emptyReserveTile.transform.position) <= 0.1f)
        {
            hasSelectedCard = false;
            return;
        }
        if(!GameLogic.Instance.InInventory.Contains(selectedCard))
        {
            GameLogic.Instance.InInventory.Add(selectedCard);
        }
        offerAvailable = false;
    }
    void DestroyRemainingCards()
    {
        OfferedCards.Remove(selectedCard);
        foreach (GameObject Card in OfferedCards)
        {
            GameLogic.Instance.InInventory.Remove(Card);
            Destroy(Card);
        }
    }
}
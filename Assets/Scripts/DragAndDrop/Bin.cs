﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bin : MonoBehaviour
{
    [SerializeField] List<GameObject> CardsInBin = new List<GameObject>();
    bool isMovingToBin = false;
    void Update()
    {
        MoveToBinSlot();
    }

    void MoveToBinSlot()
    {
        foreach (GameObject Card in CardsInBin)
        {
            Card.transform.position = Vector3.Lerp(Card.transform.position, transform.position, PointAndClickLogic.Instance.cardMoveSpeed * Time.deltaTime);
        }
    }

    private void OnMouseOver()
    {
        if(Input.GetMouseButtonDown(0) && !isMovingToBin)
        {
            if(PointAndClickLogic.Instance.hasStartSlot && !CardsInBin.Contains(PointAndClickLogic.Instance.CardToMove) && PointAndClickLogic.Instance.isFromReserve)
            {
                CardsInBin.Add(PointAndClickLogic.Instance.CardToMove);
                PointAndClickLogic.Instance.StartSlot.GetComponent<PointAndClick>().PlacedCard.transform.parent = gameObject.transform;
                PointAndClickLogic.Instance.StartSlot.GetComponent<PointAndClick>().PlacedCard = null;
                PointAndClickLogic.Instance.StartSlot.GetComponent<PointAndClick>().hasPlacedCard = false;
                PointAndClickLogic.Instance.SetClear();
                StartCoroutine(EmptyBin());
            }
        }
    }

    IEnumerator EmptyBin()
    {
        isMovingToBin = true;
        yield return new WaitForSeconds(1);
        foreach(GameObject Card in CardsInBin)
        {
            Destroy(Card);
        }
        CardsInBin.Clear();
        isMovingToBin = false;
    }

    /*public void EmptyBin()
    {
        foreach (GameObject Card in CardsInBin)
        {
            Destroy(Card);
        }
    }*/
}

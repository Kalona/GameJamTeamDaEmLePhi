﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class symbol_trigger : MonoBehaviour
{
    CardVar ParentScript;
    SpriteRenderer Sprite_sprite;

    void Start()
    {
        ParentScript = gameObject.transform.parent.parent.gameObject.GetComponent<CardVar>();
        Sprite_sprite = gameObject.GetComponent<SpriteRenderer>();
    }


    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Symbol")
        {
            ParentScript.Neighbors.Remove(other.gameObject.transform.parent.parent.gameObject);
            Symbol_ActiveOrInactive(other, -1, GameLogic.Instance.ArtArsenal.Symbols_activated, GameLogic.Instance.ArtArsenal.Symbols);

        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Connection")
        {
            if (ProgrammedAnimations.Instance.Allow_Connections)
            {
                if (GameLogic.Instance.ArtArsenal.Symbols_activated.Contains(Sprite_sprite.sprite))
                {
                    other.gameObject.transform.GetComponent<MeshRenderer>().material = GameLogic.Instance.ArtArsenal.Active_Connection;
                    //Dann wird dessen child auf active gesetzt, aka das Blitzen
                    other.gameObject.transform.GetChild(0).transform.gameObject.SetActive(true);
                }
            }

            if (GameLogic.Instance.ArtArsenal.Symbols_activated.Contains(Sprite_sprite.sprite) == false)
            {
                other.gameObject.transform.GetComponent<MeshRenderer>().material = GameLogic.Instance.ArtArsenal.Inactive_Connection;
                other.gameObject.transform.GetChild(0).transform.gameObject.SetActive(false);
            }


        }

        if (other.gameObject.tag == "Symbol") //Bugfix for when symbols don't get triggered even though they should - maybe can use it instead of ontrigger enter?
        {
            //Activate symbols
            if (GameLogic.Instance.InInventory.Contains(ParentScript.gameObject) == false && GameLogic.Instance.InInventory.Contains(other.transform.parent.parent.gameObject) == false) //Only Make symbols react if this Card or its triggerer is not in the inventory
            {
                if (other.gameObject.GetComponent<SpriteRenderer>().sprite == Sprite_sprite.sprite) //Checks if symbols are the same)
                {

                    //for activated
                    if (Sprite_sprite.sprite.ToString().Contains("activated") == false)
                    {
                        for (int s = 0; s < GameLogic.Instance.ArtArsenal.Symbols.Count; s++)
                        {
                            if (Sprite_sprite.sprite == GameLogic.Instance.ArtArsenal.Symbols[s])
                            {
                                //Changes Sprites to new ones
                                Sprite_sprite.sprite = GameLogic.Instance.ArtArsenal.Symbols_activated[s];
                                other.gameObject.GetComponent<SpriteRenderer>().sprite = GameLogic.Instance.ArtArsenal.Symbols_activated[s];
                                //Adds or removes Points
                                GameLogic.Instance.AddSubstract_Points(1, gameObject.transform.parent.parent.gameObject);
                                GameLogic.Instance.AddSubstract_Points(1, other.gameObject.transform.parent.parent.gameObject);

                            }
                        }
                    }
                }
            }


        }
    }


    void Symbol_ActiveOrInactive(Collider other, int exitenter, List<Sprite> OldList, List<Sprite> NewList)
    {
        //only trigger when not in inventory
        if (other.gameObject.GetComponent<SpriteRenderer>().sprite == Sprite_sprite.sprite) //Checks if symbols are the same
        {
            for (int s = 0; s < GameLogic.Instance.ArtArsenal.Symbols.Count; s++)
            {
                if (Sprite_sprite.sprite == OldList[s])
                {
                    //Changes Sprites to new ones
                    Sprite_sprite.sprite = NewList[s];
                    other.gameObject.GetComponent<SpriteRenderer>().sprite = NewList[s];
                    //Adds or removes Points
                    GameLogic.Instance.AddSubstract_Points(exitenter, gameObject.transform.parent.parent.gameObject);
                    GameLogic.Instance.AddSubstract_Points(exitenter, other.gameObject.transform.parent.parent.gameObject);

                }
            }

        }
    }
}


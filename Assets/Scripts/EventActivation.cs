﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventActivation : MonoBehaviour
{
    [SerializeField] List<Animator> myAnims = new List<Animator>();

    //[SerializeField] Animator myAnim;

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(1))
        {
            foreach (Animator myAnim in myAnims)
            {
                myAnim.SetTrigger("MoveUp");
            }
        }
    }
}

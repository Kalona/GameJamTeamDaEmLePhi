﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Display : MonoBehaviour
{

    private Material _currentMaterial;
    public enum SkillType { MainSkill, SubSkillOne, SubSkillTwo };

    public SkillType type;

    private EventSkills _myEvent;

    [SerializeField] private TMP_Text skillText;

    private string _skillString;

    private void Start()
    {
        CheckForColorChange();
    }

    private void LateUpdate()
    {
        if (EventManager.Instance.changeColor)
        {
            CheckForColorChange();
            UpdateScore();
        }
    }

    void UpdateScore()
    {
        switch (type)
        {
            case SkillType.MainSkill:
                _skillString = EventManager.Instance.activeSkills.rdmNumberMain.ToString();
                skillText.text = _skillString;
                break;
            case SkillType.SubSkillOne:
                _skillString = EventManager.Instance.activeSkills.rdmNumberSubOne.ToString();
                skillText.text = _skillString;
                break;
            case SkillType.SubSkillTwo:
                _skillString = EventManager.Instance.activeSkills.rdmNumberSubTwo.ToString();
                skillText.text = _skillString;
                break;
        }
    }
    public void CheckForColorChange()
    {
        EventSkills.Skills skills = EventSkills.Skills.None;
        List<GameObject> bottle = null;
        int PointsLeft = 0;

        if (type == SkillType.MainSkill)
        {
            skills = EventManager.Instance.activeSkills.MainSkill;
            bottle = GameLogic.Instance.ArtArsenal.Main_bottle;
            PointsLeft = GameLogic.Instance.Main_PointsLeft;
        }
        if (type == SkillType.SubSkillOne)
        {
            skills = EventManager.Instance.activeSkills.SubSkillOne;
            bottle = GameLogic.Instance.ArtArsenal.Sub_bottle;
            PointsLeft = GameLogic.Instance.Sub_PointsLeft;
        }
        if (type == SkillType.SubSkillTwo)
        {
            skills = EventManager.Instance.activeSkills.SubSkillTwo;
            bottle = GameLogic.Instance.ArtArsenal.Sub_Two_bottle;
            PointsLeft = GameLogic.Instance.Sub_two_PointsLeft;
        }

        switch (type)
        {
            case SkillType.MainSkill:
                break;
            case SkillType.SubSkillOne:
                break;
            case SkillType.SubSkillTwo:
                break;
        }

        switch (skills)
        {
            case EventSkills.Skills.Anger:
                ChangeColor(bottle, GameLogic.Instance.ArtArsenal.Inactive_Bar[0], GameLogic.Instance.ArtArsenal.Active_Bar[0],PointsLeft);
                break;
            case EventSkills.Skills.Sadness:
                ChangeColor(bottle, GameLogic.Instance.ArtArsenal.Inactive_Bar[1], GameLogic.Instance.ArtArsenal.Active_Bar[1], PointsLeft);
                break;
            case EventSkills.Skills.Fear:
                ChangeColor(bottle, GameLogic.Instance.ArtArsenal.Inactive_Bar[2], GameLogic.Instance.ArtArsenal.Active_Bar[2], PointsLeft);
                break;
            case EventSkills.Skills.Joy:
                ChangeColor(bottle, GameLogic.Instance.ArtArsenal.Inactive_Bar[3], GameLogic.Instance.ArtArsenal.Active_Bar[3], PointsLeft);
                break;
            case EventSkills.Skills.Disgust:
                ChangeColor(bottle, GameLogic.Instance.ArtArsenal.Inactive_Bar[4], GameLogic.Instance.ArtArsenal.Active_Bar[4], PointsLeft);
                break;
            case EventSkills.Skills.None:
                ChangeColor(bottle, GameLogic.Instance.ArtArsenal.Inactive_Bar[5], GameLogic.Instance.ArtArsenal.Active_Bar[5], PointsLeft);
                break;
        }
    }
    private void ChangeColor(List<GameObject> bottles, Material normalMaterial, Material glowMaterial, int pointsleft) //Dalas addon, also changing color of Point bar now
    {
        if(pointsleft == 0)
        {
            GetComponent<MeshRenderer>().material = normalMaterial;
            for (int i = 0; i < bottles.Count; i++)
            {
                bottles[i].GetComponent<MeshRenderer>().material = glowMaterial;
            }
        }
        else
        {
            GetComponent<MeshRenderer>().material = normalMaterial;
            for (int i = 0; i < bottles.Count; i++)
            {
                bottles[i].GetComponent<MeshRenderer>().material = normalMaterial;
            }
        }
      
    }
}
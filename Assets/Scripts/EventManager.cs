﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

//Contributer: Lenja Raschke
public class EventManager : MonoBehaviour
{
    public static EventManager Instance;
    
    [HideInInspector] public List<EventSkills> eventList;
    [HideInInspector] public EventSkills[] eventListArray;

    [SerializeField] private List<Transform> eventPositions = new List<Transform>();
    public EventSkills activeSkills;
    public List<EventSkills> _assignedEvents =  new List<EventSkills>(); 
    public float speed;

    public bool changeColor;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
        AssignEvents();
    }

  
    EventSkills InstantiateNewEvent(Transform eventPos)
    {
        eventListArray = Resources.LoadAll<EventSkills>("Events");

        eventList = eventListArray.ToList();

        EventSkills eventToSpawn = eventList[UnityEngine.Random.Range(0, eventList.Count)];
        EventSkills newEvent = Instantiate(eventToSpawn, eventPos.position, Quaternion.identity);

        return newEvent;
    }

    void AssignEvents()
    {
        for (int i = 0; i <= eventPositions.Count -1; i++)
        {
            EventSkills newEvent = InstantiateNewEvent(eventPositions[i]);
            newEvent.name = "Event " + i;
            _assignedEvents.Add(newEvent);
        }
        SetActiveSkill(0);
    }

    public void SetActiveSkill(int Rank)
    {
        activeSkills = _assignedEvents[2];
        changeColor = true;
        GameLogic.Instance.Eventpoint_calculation();
        GameLogic.Instance.Count_ForRealThisTime(); //Count after new Event is shows but before new Cards are created to get the PointBar correctly updated to the new event
        if(GameLogic.Instance.round!=0)
        {
            CardOffer.Instance.GenerateOffer(Rank); //New cards available
        }

    }

    
    void SwapR(Transform x, EventSkills y)
    {
        while (y.transform.position != x.transform.position) //endless loop bc x never reaches y
        {
            y.transform.position = Vector3.MoveTowards(y.transform.position, x.transform.position, speed * Time.deltaTime );
            
        }
        
    }
    
    void Swap(EventSkills x, EventSkills y)
    {
        float timer = 0f;
        EventSkills temp = x;
        
        while (timer <= 10f)
        {
            temp.transform.position = Vector3.Lerp(x.transform.position, y.transform.position, speed * Time.deltaTime);
            timer += Time.deltaTime;
        }
        timer = 0f; 
        temp.transform.position = y.transform.position;
        temp = y;
        y = temp;
    } //disabled rn
    public void SwapEvents()
    {
        //AudioManager.instance.PlaySound("RackMovement");
        for (int i =0; i < _assignedEvents.Count -1; i++)//D: go through each event
        {
            Transform EventStartPos = _assignedEvents[i+1].transform;
            SwapR(EventStartPos, _assignedEvents[i]); //take each event and the next one
            //Swap(_assignedEvents[i], _assignedEvents[i+1]);
            
        }
        var newEvent = InstantiateNewEvent(eventPositions[0]);
        _assignedEvents.Insert(0, newEvent);

        //Philipps Stuff
        GameLogic.Instance.remainingActions = GameLogic.Instance.maxActions;    //New actions available
        PointAndClickLogic.Instance.isExecutingDelayedActions = true;   //Execute Delayed Actions
    }
    
}
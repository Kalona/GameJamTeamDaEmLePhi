﻿using NaughtyAttributes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using Unity.IO.LowLevel.Unsafe;
using UnityEngine;

public class GameLogic : MonoBehaviour
{
    [Header("References")]
    public static GameLogic Instance;
    public ArtReferences ArtArsenal;
    public InventoryMovement InventoryScript;
    [SerializeField] public TMP_Text Round;
    [SerializeField] public TMP_Text Actionsleft;


    [Header("Skills")]
    public List<string> Skills = new List<string>();
    [Header("Cards")]
    [HideInInspector]
    public GameObject[] AllCard;
    [ReadOnly]
    public List<GameObject> InInventory = new List<GameObject>();
    List<GameObject> CountingCard = new List<GameObject>();
    public List<GameObject> CopiedCards = new List<GameObject>();
    [Header("Actions")]
    public int maxActions;
    public int remainingActions;

    [Header("Current Point Score")]
    public int Lives_Left; //Starts with biggest number and get's smaller whenver you failed to get Rank1
    [ReadOnly]
    public int Main_PointsLeft; //Goalpoints - points on played cards 
    [ReadOnly]
    public int Sub_PointsLeft;
    [ReadOnly]
    public int Sub_two_PointsLeft;
    //current points on the board
    int anger_points;
    int sadness_points;
    int fear_points;
    int joy_points;
    int disgust_points;

    [Header("Event calculation")]
    public int round = -1;
    public int Basis = 9;
    public int MainPercent;
    public int SubPercent;
    public int SubSubPercent;
    [Header("Rank based on percent")]
    public float Rank_OneStar;
    public float Rank_TwoStar;
    public float Rank_ThreeStar;
    public int Current_Rank = 1; //Use it for brain sizes 
    [Header("SkillLevel Ranges")] //Skillevel depending on how well you did in this round. variables are min and max of your card numbers
    [SerializeField] int OneStar_min;
    [Space]
    [SerializeField] int TwoStar_min;
    [Space]
    [SerializeField] int ThreeStar_min;

    public bool canSnap = true;

    public WinLose winLose;

    public Vector3 spawn_point;
    int prev_max; //ensures subskill is always lower than main skill
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }
    private void Start()
    {
        StartCoroutine(PhysicsSnapCooldown());
        Eventpoint_calculation();
        Count_ForRealThisTime();

    }

    IEnumerator PhysicsSnapCooldown()
    {
        yield return new WaitForSeconds(5);
        canSnap = false;
    }
    void Update()
    {
        //EventManager.Instance.changeColor = false;
        Update_Texts();

        if (remainingActions == 0)
        {
            AudioManager.instance.PlaySound("ButtonPress");
            ProgrammedAnimations.Instance.buttonpress = true;
            ProgrammedAnimations.Instance.kastentimer = 0;
            ProgrammedAnimations.Instance.kasten = true;
            Dependancies(); //new Event

            if (Current_Rank == 0)
            {
                winLose.roundfailed++;

            }
        }

    }

    void Dependancies()
    {
        Count_ForRealThisTime(); //Get the rank for the cards
                                 //Get next event + a card
        EventManager.Instance.SwapEvents();
        EventManager.Instance.SetActiveSkill(Current_Rank);
    }

    public void CreateCard(int Rank, Vector3 position, bool rewardCard) //Rank is based on how good you finished the Level! 1= one Star (terriblle but still ok), 2 =Two stars(neutral), 3= three and max amoutn of stars
    {
        //Instantiate Card
        GameObject CopiedCard = Instantiate(ArtArsenal.Card_Base, position, transform.rotation * Quaternion.Euler(0f, 0f, 45));
        MeshRenderer Mesh = CopiedCard.transform.GetChild(0).gameObject.transform.GetChild(4).gameObject.GetComponent<MeshRenderer>();
        GameObject MainSkillNumber = CopiedCard.transform.GetChild(1).gameObject;
        CardVar CopiedCardVar = CopiedCard.GetComponent<CardVar>(); //Get Script (with all the variables in it) of the Card

        if (CardOffer.Instance.offerAvailable)
        {
            CardOffer.Instance.OfferedCards.Add(CopiedCard);
        }

        //Randomize Skills
        CopiedCardVar.mainSkill = Skills[UnityEngine.Random.Range(0, Skills.Count)];

        // Randomize SkillLevels (depending on Rank aka how well you did in the Event)
        switch (Rank)
        {
            case 0:
                break;
            case 1:
                Randomize_SkillLevels(MainSkillNumber, OneStar_min, OneStar_min);
                break;
            case 2:
                Randomize_SkillLevels(MainSkillNumber, TwoStar_min, TwoStar_min);
                break;
            case 3:
                Randomize_SkillLevels(MainSkillNumber, ThreeStar_min, ThreeStar_min);
                break;
            case 4:
                Randomize_SkillLevels(MainSkillNumber, ThreeStar_min, ThreeStar_min);
                break;
        }

        //Randomize Image depending on mainSkill
        switch (CopiedCardVar.mainSkill)
        {
            case "Anger":
                Mesh.material = ArtArsenal.Images_Anger;
                break;
            case "Sadness":
                Mesh.material = ArtArsenal.Images_Sadness;
                break;
            case "Fear":
                Mesh.material = ArtArsenal.Images_Fear;
                break;
            case "Joy":
                Mesh.material = ArtArsenal.Images_Joy;
                break;
            case "Disgust":
                Mesh.material = ArtArsenal.Images_Disgust;
                break;
        }
        //RandomizeSymbols
        Randomize_Symbols(CopiedCard);
        CopiedCard.name = (CopiedCardVar.mainSkill);
        //Bugfix
        if (rewardCard == true)
        {
            InInventory.Add(CopiedCard);
        }
    }

    void Randomize_SkillLevels(GameObject MainOrSub, int minLevel, int maxLevel)
    {
        prev_max = UnityEngine.Random.Range(minLevel, maxLevel);
        MainOrSub.GetComponent<TMP_Text>().text = prev_max.ToString();
        if (MainOrSub.GetComponent<TMP_Text>().text == 0.ToString()) //if Sub skill has a level of zero
        {
            MainOrSub.SetActive(false);
        }
    }
    void Randomize_Symbols(GameObject CopiedCard)
    {
        GameObject Symbols = CopiedCard.transform.GetChild(2).gameObject;
        foreach (Transform symbols in Symbols.transform)
        {
            symbols.GetComponent<SpriteRenderer>().sprite = ArtArsenal.Symbols[UnityEngine.Random.Range(0, ArtArsenal.Symbols.Count)];
        }
    }

    public void CopyCard(GameObject CardToCopy, Vector3 CopyCardPosition, GameObject AttachedSlot) //Creates a copy that has NO EFFECT in the game, but is just the picture of a card
    {
        GameObject CopiedCard = Instantiate(CardToCopy, CopyCardPosition, CardToCopy.transform.rotation); //Copies Card easy-peasy
        CopiedCards.Add(CopiedCard);
        //Now do the black magic
        CopiedCard.transform.parent = AttachedSlot.transform;
        CopiedCard.transform.localScale *= 0.7f;
        AttachedSlot.GetComponent<PointAndClick>().CardPreshow = CopiedCard;
        AttachedSlot.GetComponent<PointAndClick>().hasCardPreshow = CopiedCard;
        Destroy(CopiedCard.GetComponent<Collider>());
        Destroy(CopiedCard.GetComponent<Rigidbody>());
        CopiedCard.tag = "Untagged";

        for (int w = 0; w < 4; w++)
        {
            Destroy(CopiedCard.transform.GetChild(2).GetChild(w).GetComponent<Collider>());
            if (ArtArsenal.Symbols_activated.Contains(CopiedCard.transform.GetChild(2).GetChild(w).GetComponent<SpriteRenderer>().sprite)) //if one of the sybols is active
            {
                AddSubstract_Points(-1, CopiedCard); //Reset to standard number
                CopiedCard.transform.GetChild(2).GetChild(w).GetComponent<SpriteRenderer>().sprite = ArtArsenal.Symbols[w - 1]; //Set it back to inactive
            }
        }
        Destroy(CopiedCard.GetComponent<CardVar>()); //Destroy script just to make sure it won't mess with anything
    }

    public void Count_AllPoints()
    {
        //Count all Cards that are NOT IN INVETNORY, ARE NOT BEING HOLD RN, AND NOT GIFT CARDS
        CountingCard = GameObject.FindGameObjectsWithTag("Card").ToList<GameObject>();
        foreach (GameObject InventoryCard in InInventory)
        {
            CountingCard.Remove(InventoryCard); //Inventory Card Points don't get counted
        }
        anger_points = 0;
        sadness_points = 0;
        fear_points = 0;
        joy_points = 0;
        disgust_points = 0;
        foreach (GameObject cards in CountingCard)
        {
            CardVar cardsScript = cards.GetComponent<CardVar>();
            anger_points += cardsScript.skillLevel_anger;
            sadness_points += cardsScript.skillLevel_sadness;
            fear_points += cardsScript.skillLevel_fear;
            joy_points += cardsScript.skillLevel_joy;
            disgust_points += cardsScript.skillLevel_disgust;
        }
    }

    void Count_CompareToEvent(EventSkills.Skills skillHierachy, int eventPoints)
    {
        int goalPoints = 0;
        switch (skillHierachy)
        {
            case EventSkills.Skills.Anger:
                goalPoints = eventPoints - anger_points;
                break;
            case EventSkills.Skills.Sadness:
                goalPoints = eventPoints - sadness_points;
                break;
            case EventSkills.Skills.Fear:
                goalPoints = eventPoints - fear_points;
                break;
            case EventSkills.Skills.Joy:
                goalPoints = eventPoints - joy_points;
                break;
            case EventSkills.Skills.Disgust:
                goalPoints = eventPoints - disgust_points;
                break;

        }
        if (Mathf.Sign(goalPoints) == -1) //if you have more than enough points, don't add them up but just say you have 0 left!
        {
            goalPoints = 0;
        }
        if (skillHierachy == EventManager.Instance.activeSkills.MainSkill)
        {
            Main_PointsLeft = goalPoints;
        }
        if (skillHierachy == EventManager.Instance.activeSkills.SubSkillOne)
        {
            Sub_PointsLeft = goalPoints;
        }
        if (skillHierachy == EventManager.Instance.activeSkills.SubSkillTwo)
        {
            Sub_two_PointsLeft = goalPoints;
        }
    }
    public void Count_ForRealThisTime() //Updates after every move or when all actions are over
    {
        Count_AllPoints();
        Count_CompareToEvent(EventManager.Instance.activeSkills.MainSkill, EventManager.Instance.activeSkills.rdmNumberMain); //Gets goal number
        Count_CompareToEvent(EventManager.Instance.activeSkills.SubSkillOne, EventManager.Instance.activeSkills.rdmNumberSubOne);
        Count_CompareToEvent(EventManager.Instance.activeSkills.SubSkillTwo, EventManager.Instance.activeSkills.rdmNumberSubTwo);
        Get_Rank_Set_Liquid(ArtArsenal.Main_bottle, EventManager.Instance.activeSkills.rdmNumberMain, Main_PointsLeft); //Sets liquid properly
        Get_Rank_Set_Liquid(ArtArsenal.Sub_bottle, EventManager.Instance.activeSkills.rdmNumberSubOne, Sub_PointsLeft);
        Get_Rank_Set_Liquid(ArtArsenal.Sub_Two_bottle, EventManager.Instance.activeSkills.rdmNumberSubTwo, Sub_two_PointsLeft);
    }

    public void Eventpoint_calculation()
    {
        //Randomize Next Event Numbers, based on how many Cards you own + your Last Rank
        AllCard = GameObject.FindGameObjectsWithTag("Card"); // -3 because the Reward cards are on the board as well
        round += 1; //made it one round further
        Eventpoint_randomizer();

    }

    public void Eventpoint_randomizer()
    {
        int BasisRest = Basis; //Basis wird in Prozent aufgeteilt. 50-60. 30-40, 10-20
        int MainEvent = UnityEngine.Random.Range(Mathf.RoundToInt((Basis * MainPercent) / 100), Mathf.RoundToInt((Basis * (MainPercent + 10)) / 100));
        EventManager.Instance.activeSkills.rdmNumberMain = MainEvent;
        int SubEvent = UnityEngine.Random.Range(Mathf.RoundToInt((Basis * SubPercent) / 100), Mathf.RoundToInt((Basis * (SubPercent + 10)) / 100));
        BasisRest = BasisRest - MainEvent - SubEvent; //what is left from the prev events
        //Set Event Points to 0 if Subskills have no emotions!
        if (EventManager.Instance.activeSkills.SubSkillOne == EventSkills.Skills.None)
        {
            EventManager.Instance.activeSkills.rdmNumberSubOne = 0;
        }
        else
        {
            EventManager.Instance.activeSkills.rdmNumberSubOne = SubEvent;
        }

        if (EventManager.Instance.activeSkills.SubSkillTwo == EventSkills.Skills.None)
        {
            EventManager.Instance.activeSkills.rdmNumberMain += BasisRest; //Main event gets the Rest
            EventManager.Instance.activeSkills.rdmNumberSubTwo = 0;
        }
        else
        {
            EventManager.Instance.activeSkills.rdmNumberSubTwo = BasisRest;
        }

        if(round <10)
        {
            Basis += 2; //nächste runde wirds schwieriger
        }
        
    }
    void Get_Rank_Set_Liquid(List<GameObject> Bottles, int goalnumber, int pointleft)
    {
        int GoalPoints_together = (EventManager.Instance.activeSkills.rdmNumberMain + EventManager.Instance.activeSkills.rdmNumberSubOne + EventManager.Instance.activeSkills.rdmNumberSubTwo);
        int BoardPoints_left = Main_PointsLeft + Sub_PointsLeft + Sub_two_PointsLeft;
        if (BoardPoints_left <= GoalPoints_together * ((100 - Rank_OneStar) / 100)) //acceptable Rank but still bad
        {
            Current_Rank = 1;
        }
        else
        {
            Current_Rank = 0; //you have less than points than the lowest Rank allows. You loose one live + right now you also get a card with zero points but that is just a working bug************************
        }
        if (BoardPoints_left <= GoalPoints_together * ((100 - Rank_TwoStar) / 100))
        {
            Current_Rank = 2;
        }
        if (BoardPoints_left <= GoalPoints_together * ((100 - Rank_ThreeStar) / 100)) //you have more tha Rank_ThreeStar percent of all points, whooo!
        {
            Current_Rank = 3;
        }
        if (BoardPoints_left == 0) //you got 100% right
        {
            Current_Rank = 4;
        }

        //Liquid according to your points
        //Remap from 0 - 10, for whatever reason I can't do it in one go. idk ill check it out later
        float a = goalnumber - pointleft;
        float b = ((a) / goalnumber) * 100;
        b = Mathf.Round(b / 10) * 10;

        for (int l = 0; l < Bottles.Count; l++)
        {
            if (l >= b / 10)
            {
                Bottles[l].SetActive(false);
            }
            else
            {
                Bottles[l].SetActive(true);
            }

        }
    }

    void Update_Texts() //Round and Actions
    {
        Round.text = round.ToString();
        Actionsleft.text = remainingActions.ToString();
    }
    public void AddSubstract_Points(int exitOrenter, GameObject thisOrthatCard) //variables: if you entered or exit a kombi, and with whom you hav this kombi
    {
        CardVar KombiCardScript = thisOrthatCard.GetComponent<CardVar>();
        //look after my highest points and neighbors highest points
        //check whats my main skill and add Boni to it
        switch (KombiCardScript.mainSkill)
        {
            case "Anger":
                KombiCardScript.skillLevel_anger += exitOrenter;
                thisOrthatCard.transform.GetChild(1).GetComponent<TMP_Text>().text = KombiCardScript.skillLevel_anger.ToString();
                break;
            case "Sadness":
                KombiCardScript.skillLevel_sadness += exitOrenter;
                thisOrthatCard.transform.GetChild(1).GetComponent<TMP_Text>().text = KombiCardScript.skillLevel_sadness.ToString();
                break;
            case "Fear":
                KombiCardScript.skillLevel_fear += exitOrenter;
                thisOrthatCard.transform.GetChild(1).GetComponent<TMP_Text>().text = KombiCardScript.skillLevel_fear.ToString();
                break;
            case "Joy":
                KombiCardScript.skillLevel_joy += exitOrenter;
                thisOrthatCard.transform.GetChild(1).GetComponent<TMP_Text>().text = KombiCardScript.skillLevel_joy.ToString();
                break;
            case "Disgust":
                KombiCardScript.skillLevel_disgust += exitOrenter;
                thisOrthatCard.transform.GetChild(1).GetComponent<TMP_Text>().text = KombiCardScript.skillLevel_disgust.ToString();
                break;
        }
    }
}

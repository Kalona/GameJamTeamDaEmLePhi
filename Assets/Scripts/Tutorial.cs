﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Contributer: Emma Steiner
public class Tutorial : MonoBehaviour
{
    public GameLogic gameLogic;
    public CardOffer cardOffer;
    public ProgrammedAnimations progAnim;

    //Windows for Tutorial
    [Header("Tutorial")]
    public GameObject willkommen;
    public GameObject events;
    public GameObject cards;
    public GameObject placeMent;
    public GameObject endTurn;
    public GameObject belohnung;
    public GameObject ablage;

    [Header("Helpers")]
    public GameObject eventCol;
    public GameObject cardsCol;
    public GameObject cardMoveCol;
    public GameObject endTurnCol;
    public GameObject rewardCol;
    public GameObject inventCol;

    //Buttons
    [Header("ButtonsToShow")]
    public GameObject buttonPlacement;
    public GameObject buttonEndButt;
    public GameObject buttonBelohnung;

    //BGSwitch
    [Header("BGSwitches")]
    public GameObject cardMove1;
    public GameObject cardMove2;
    //
    public GameObject endTurnOverl1;
    public GameObject endTurnOverl2;
    //
    public GameObject rewardOverl1;
    public GameObject rewardOverl2;

    //Collider
    [Header("Collider")]
    public GameObject colliderNoZuge;
    public GameObject colliderNoRewards;
    public GameObject colliderNoButton;


    //Collider
    [Header("ShowOnBoard")]
    public GameObject showButton;
    public GameObject showBelohung;

    // Inventory
    [Header("Inventory")]
    public GameObject inventory;

    // Cards
    [Header("Karten")]
    public GameObject starterCards;
    public GameObject fearCard;
    public GameObject happyCard;
    public GameObject sadCard;
    public GameObject disgustCard;
    public GameObject angerCard;


    //Anzeigen
    [Header("AnzeigenKarten")]
    public GameObject fearAnz;
    public GameObject happyAnz;
    public GameObject sadAnz;
    public GameObject disgustAnz;
    public GameObject angerAnz;






    private void Awake()
    {
        starterCards.SetActive(false);
        /*fearCard.SetActive(false);
        happyCard.SetActive(false);
        sadCard.SetActive(false);
        disgustCard.SetActive(false);
        angerCard.SetActive(false);*/

        willkommen.SetActive(true);

        inventory.SetActive(false);

        buttonPlacement.SetActive(false);

        cardMove1.SetActive(true);
        cardMove2.SetActive(false);

        endTurnOverl1.SetActive(true);
        endTurnOverl2.SetActive(false);

        rewardOverl1.SetActive(true);
        rewardOverl2.SetActive(false);

    }
    public void Update()
    {

        ActivateButtonMovement();
        ActivateButtonEndButton();
        ActivateSelectCardButton();
        
    }

    //Activate Buttons when XY happens//

    void ActivateButtonMovement()
    {
        if (gameLogic.remainingActions == 19)
        {
            buttonPlacement.SetActive(true);
            colliderNoZuge.SetActive(true);

            cardMove2.SetActive(true);
            cardMove1.SetActive(false);
        }

    }
    void ActivateButtonEndButton()
    {
        if (progAnim.buttonpress == true)
        {
            buttonEndButt.SetActive(true);
            //showButton.SetActive(false);
            colliderNoRewards.SetActive(true);

            endTurnOverl1.SetActive(false);
            endTurnOverl2.SetActive(true);

        }

    }
    void ActivateSelectCardButton()
    {
        if (cardOffer.hasSelectedCard == true)
        {
            buttonBelohnung.SetActive(true);
            //showBelohung.SetActive(false);

            colliderNoButton.SetActive(true);
            //colliderButtPressed.SetActive(true);

            rewardOverl1.SetActive(false);
            rewardOverl2.SetActive(true);
        }
       
    }

    //For Buttons//

    public void ToEvents()
    {
        willkommen.SetActive(false);
        events.SetActive(true);
        eventCol.SetActive(true);

        

    }

    public void ToCards()
    {
        events.SetActive(false);
        eventCol.SetActive(false);

        cards.SetActive(true);

        cardsCol.SetActive(true);


        //spawnCards
        starterCards.SetActive(true);

        /*fearCard.SetActive(true);
        happyCard.SetActive(true);
        sadCard.SetActive(true);
        disgustCard.SetActive(true);
        angerCard.SetActive(true);*/

        //spawn Inventory
        inventory.SetActive(true);


    }

    void SpawnCards()
    {

        //AddTimer

    }

    public void ToPlaceMent()
    {
        cards.SetActive(false);
        placeMent.SetActive(true);

        cardsCol.SetActive(false);
        cardMoveCol.SetActive(true);
        

        


    }
    public void ToEndTurn()
    {
        placeMent.SetActive(false);
        endTurn.SetActive(true);

        cardMoveCol.SetActive(false);
        endTurnCol.SetActive(true);

    }
    public void ToBelohnung()
    {
        

        endTurn.SetActive(false);
        belohnung.SetActive(true);

        endTurnCol.SetActive(false);
        rewardCol.SetActive(true);

    }
    public void ToAblage ()
    {
        belohnung.SetActive(false);
        ablage.SetActive(true);

        rewardCol.SetActive(false);
        inventCol.SetActive(true);


    }
    public void StartGame()
    {
        ablage.SetActive(false);
        willkommen.SetActive(false);

        inventCol.SetActive(false);

        inventory.SetActive(true);
        starterCards.SetActive(true);

    }


}

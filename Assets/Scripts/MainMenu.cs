﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//Contributer: Emma Steiner

public class MainMenu : MonoBehaviour
{
    public GameObject maineMenu;
    public GameObject credits;
    public Animator animator;

    private void Awake()
    {
        maineMenu.SetActive(true);
        //credits.SetActive(false);
        animator.SetBool("StartGame", false);
        
    }

   
    public void StartGame()
    {
        //play animation
        //close this scene
        //start gameScene with tutorial
        

        Debug.Log("Game Scene with Tutorial loaded");


    }
    public void QuitGame()
    {

        Application.Quit();
    }

    public void ShowCredits()
    {
        //show credits
        Debug.Log("Show Credits");
        credits.SetActive(true);
        animator.SetBool("MoveToCredits", true);


    }
    public void ShowMainMenu()
    {
        //Show Main Menu
        animator.SetBool("MoveToCredits", false);


    }
   


}

﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CardVar : MonoBehaviour
{
    // Start is called before the first frame update
    public string mainSkill;
    //variables only used as indicator what skillevel you have
    public int skillLevel_anger;
    public int skillLevel_sadness;
    public int skillLevel_fear;
    public int skillLevel_joy;
    public int skillLevel_disgust;
    //with who I can switch places
    public List<GameObject> Neighbors = new List<GameObject>();
    public List<GameObject> lastNeighbors = new List<GameObject>();
    //has it been counted yet?
    [HideInInspector]
    public bool counted;
    //Material detection of Slotholder
    //Dala's stuff to change Slotholders Material
    Material Slotholder_material;

    [SerializeField] float scaleCorrection = 2;

    //Philipps stuff for delayed Movement
    public GameObject StartSlot;
    public GameObject DelayedTarget;
    public GameObject CardPreshow;
    public bool cardIsFromReserve;
    public bool isDelayedSwopped;
    public bool isRemovedFromSlot = false;

    void Start()
    {
        Get_CurrentPoints(); //Put it later after each player switch and not in update!
        transform.localScale = new Vector3(scaleCorrection, scaleCorrection, scaleCorrection);

    }
    // Update is called once per frame
    void Update()
    {
    }

    public void SetClear()
    {
        DelayedTarget.GetComponent<PointAndClick>().isTargetOfDelayedMove = false;
        DelayedTarget.GetComponent<PointAndClick>().hasCardPreshow = false;
        StartSlot = null;
        DelayedTarget = null;
        isDelayedSwopped = false;
    }

    //You can probably merge Get_CurrentPoints and the AddandSubtract Function, so check that out later **********************************
    public void Get_CurrentPoints() //just for overview and maybe we can use it later on
    {
        //get what skill number is being displayed,turn to int and show in Cards Script
        switch (mainSkill)
        {
            case "Anger":
                int.TryParse(transform.GetChild(1).GetComponent<TMP_Text>().text, out skillLevel_anger);
                break;
            case "Sadness":
                int.TryParse(transform.GetChild(1).GetComponent<TMP_Text>().text, out skillLevel_sadness);
                break;
            case "Fear":
                int.TryParse(transform.GetChild(1).GetComponent<TMP_Text>().text, out skillLevel_fear);
                break;
            case "Joy":
                int.TryParse(transform.GetChild(1).GetComponent<TMP_Text>().text, out skillLevel_joy);
                break;
            case "Disgust":
                int.TryParse(transform.GetChild(1).GetComponent<TMP_Text>().text, out skillLevel_disgust);
                break;
        }

    }


}


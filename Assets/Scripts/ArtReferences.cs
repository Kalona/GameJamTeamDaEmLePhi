﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class ArtReferences : MonoBehaviour
{
    public GameObject Card_Base; //insert newest base card prefab here
    [Header("___SKILLS___")]
    public List<Sprite> Skill_Colorcircles = new List<Sprite>();
    [Header("___EFFECTS___")]
    public GameObject[] All_Holders;
    public LayerMask Layer_of_holders;
    public Material Normal_Slot;
    public Material Picked_Slot;
    [Header("___SYMBOLS___")]
    public List<Sprite> Symbols = new List<Sprite>(); //Storing all possible Symbols
    [InfoBox("Both symbol List have to be in the same order. e.g. Symbols[0]=circle => Symbols_activated[0] = circle_activated", EInfoBoxType.Warning)]
    public List<Sprite> Symbols_activated = new List<Sprite>(); //Symbols if activated
    public Material Active_Connection; //for the pipes/nerves inbetween
    public Material Inactive_Connection; //
    [Header("___LIQUID BOTTLES___")] //parent with all its children go in here
    public List<Material> Inactive_Bar = new List<Material>();
    public List<Material> Active_Bar = new List<Material>();
    public List<GameObject> Main_bottle = new List<GameObject>();
    public List<GameObject> Sub_bottle = new List<GameObject>();
    public List<GameObject> Sub_Two_bottle = new List<GameObject>();
    [HorizontalLine(color: EColor.Red)]
    [Header("___IMAGES___")] //Storing all images of fighters,craftsman,orientationDudes and social guys
    public Material Images_Anger;
    [HorizontalLine(color: EColor.Blue)]
    public Material Images_Sadness;
    [HorizontalLine(color: EColor.Violet)]
    public Material Images_Fear;
    [HorizontalLine(color: EColor.Yellow)]
    public Material Images_Joy;
    [HorizontalLine(color: EColor.Green)]
    public Material Images_Disgust;

    public void Start()
    {
        All_Holders = GameObject.FindGameObjectsWithTag("CardHolder");
    }

    public void Activate_Cardholder()
    {
        Deactivate_Cardholder();
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, 100.0f, Layer_of_holders))
        {
           //Deactivate_Cardholder();
            hit.transform.GetComponent<MeshRenderer>().material = Picked_Slot;
        }
    }
    public void Deactivate_Cardholder()
    {
        foreach (GameObject holder in All_Holders)
        {
            holder.GetComponent<MeshRenderer>().material = Normal_Slot;
        }
    }
}

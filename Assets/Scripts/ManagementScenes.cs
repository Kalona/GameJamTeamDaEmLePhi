﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


//Contributer: Emma Steiner
public class ManagementScenes : MonoBehaviour
{
    public void Start()
    {
        AudioManager.instance.PlaySound("Atmo");
    }

    public void LoadMainScene()
    {
        SceneManager.LoadScene(1);
        Debug.Log("MainScene");

    }

    public void QuitGame()
    {

        Application.Quit();

    }

}

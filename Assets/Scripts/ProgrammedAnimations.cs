﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProgrammedAnimations : MonoBehaviour
{
    public static ProgrammedAnimations Instance;

    [Header("Reward Board Turn")]
    public GameObject Reward_Board;
    public int spin_side = 90;
    public float spin_timer = 1;
    public AnimationCurve SpinBox_animation; //curve displays the Y rotation change of the reward board. 1 on the curve equals -180/-360 on the y rotation, 0 == 0/-180

    [Header("End Button")]
    public GameObject button;
    public GameObject Untere_Klappe;
    public GameObject Obere_Klappe;
    float obere_klappe_rot;
    public int dreh_richtung = -1;
    public bool kasten;
    public float kastentimer;
    public int trying;
    public bool buttonpress;
    public float x_startpos;

    [Header("Other Stuff")]
    public bool Allow_Connections;

    [Header("Picture boards")]
    public GameObject testObject; //later replace with a foreach eventcard, bzw card wont be enough bc you need another pivot

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }
    private void Start()
    {
        x_startpos = button.transform.localPosition.x;
        obere_klappe_rot = Obere_Klappe.transform.eulerAngles.x;
    }

    void Update()
    {
        if(kastentimer<1)
        {
            kastentimer += Time.deltaTime*5;
        }
        else
        {
            kastentimer = 1;
        }
        

        //Reward Board
       spin_timer += Time.deltaTime/3;
       Reward_Board.transform.eulerAngles = new Vector3(0, spin_side + SpinBox_animation.Evaluate(spin_timer) * -180, 0);
        if(spin_timer >1)
        {
            if (Reward_Board.transform.eulerAngles.y > 268 && Reward_Board.transform.eulerAngles.y < 272 ) //Around -90, showing frontside of the board
            {
                GameLogic.Instance.Actionsleft.gameObject.SetActive(true);
            }
        }
        //End button press
        if(buttonpress)
        {
            button.transform.localPosition = Vector3.Lerp(button.transform.localPosition, new Vector3(x_startpos+0.1f, button.transform.localPosition.y, button.transform.localPosition.z), 4f *Time.deltaTime);
            if(button.transform.localPosition.x > x_startpos+ 0.07f)
            {
                buttonpress = false;
            }
        }
        else
        {
            button.transform.localPosition = Vector3.Lerp(button.transform.localPosition, new Vector3(x_startpos, button.transform.localPosition.y, button.transform.localPosition.z), 4 * Time.deltaTime);
        }    
        
        if(kasten)
        {
            if(dreh_richtung == -1)
            {
                Untere_Klappe.transform.eulerAngles = new Vector3(Untere_Klappe.transform.eulerAngles.x, Untere_Klappe.transform.eulerAngles.y, -70 * kastentimer);
                Obere_Klappe.transform.eulerAngles = new Vector3(trying * kastentimer, Obere_Klappe.transform.eulerAngles.y, Obere_Klappe.transform.eulerAngles.z);
            }
            else
            {
                Untere_Klappe.transform.eulerAngles = new Vector3(Untere_Klappe.transform.eulerAngles.x, Untere_Klappe.transform.eulerAngles.y, 0 * kastentimer);
                Obere_Klappe.transform.eulerAngles = new Vector3(obere_klappe_rot * kastentimer, Obere_Klappe.transform.eulerAngles.y, Obere_Klappe.transform.eulerAngles.z);
            }
            
        }
    }

    public void Start_SpinRewardBoard()
    {
        spin_timer = 0;
    }
 

    
}

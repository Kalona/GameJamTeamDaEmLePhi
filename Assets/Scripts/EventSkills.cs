﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using NaughtyAttributes;
using Debug = UnityEngine.Debug;
using Random = UnityEngine.Random;

//Contributer: Lenja Raschke
public class EventSkills : MonoBehaviour
{
    public enum Skills
    {
        None = 0,
        Anger = 2,
        Joy = 4,
        Sadness = 8,
        Disgust = 16,
        Fear = 32,
    }

    [HorizontalLine(color: EColor.Red)] public Skills MainSkill;

    [HorizontalLine(color: EColor.Yellow)] public Skills SubSkillOne;

    [HorizontalLine(color: EColor.Yellow)] public Skills SubSkillTwo;

    public int rdmNumberMain;
    public int rdmNumberSubOne;
    public int rdmNumberSubTwo;
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Garbage Collection"))
        {
            this.gameObject.SetActive(false);
        } 
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class DontDestroyOnLoad : MonoBehaviour

{
  public void Awake()
  {
    GameObject[] soundtrack = GameObject.FindGameObjectsWithTag("Soundtrack");
    if (soundtrack.Length > 1)
    {
      Destroy(this.gameObject);
    }
    DontDestroyOnLoad(this.gameObject);
  }
}

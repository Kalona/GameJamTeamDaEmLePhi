﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryMovement : MonoBehaviour
{
    //ALSO HAS ALL BUTTON PRESSES INNIT - change that script later on to fit the name and place where its at ******************************
    Vector3 InventoryStartPlace;
    public bool InventoryMovesUP;
    public bool InventoryMovesDOWN;
    public int MovingTo_Y;

    //Dalas stuff for a working inventory 
    public Vector3 LastMousepos;
    public Vector3 CurrentMousepos;
    // Start is called before the first frame update
    void Start()
    {
        InventoryStartPlace = gameObject.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (CardOffer.Instance.hasSelectedCard == true)
        {
            InventoryMovesUP = false;
            InventoryMovesDOWN = true;
        }

        //Dala's inventory stuff 

        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            //Dalas stuff, moving Inventory down 
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.gameObject.name == "Inventory_Handle")
                {
                    LastMousepos = Input.mousePosition;
                }
               
                if (hit.collider.gameObject.name == "End_Button")
                {
                    GameLogic.Instance.remainingActions = 0;
                    PointAndClickLogic.Instance.SetClear();
                }
            }

             
            
        }
        if (Input.GetMouseButtonUp(0) && LastMousepos != Vector3.zero)
        {
            //Check if Player moved the mouse downwards after clicking on the Inventory Handle 
            CurrentMousepos = Input.mousePosition;
            float directionY = CurrentMousepos.y - LastMousepos.y;
            if (Mathf.Abs(directionY) > 40) //if player pulled/pushed strong enough on the handle 
            {
                if (Mathf.Sign(directionY) == 1)
                {
                    GameLogic.Instance.InventoryScript.InventoryMovesUP = true;
                    GameLogic.Instance.InventoryScript.InventoryMovesDOWN = false;
                }
                else
                {
                    GameLogic.Instance.InventoryScript.InventoryMovesUP = false;
                    GameLogic.Instance.InventoryScript.InventoryMovesDOWN = true;

                }
            }
            CurrentMousepos = Vector3.zero;
            LastMousepos = Vector3.zero;
        }

        if (InventoryMovesUP)
        {
            gameObject.transform.position = Vector3.Lerp(gameObject.transform.position, new Vector3(InventoryStartPlace.x, InventoryStartPlace.y + MovingTo_Y, InventoryStartPlace.z), 0.1f);
            
            //Push upwards
        }
        if (InventoryMovesDOWN)
        {
            gameObject.transform.position = Vector3.Lerp(gameObject.transform.position, new Vector3(InventoryStartPlace.x, InventoryStartPlace.y, InventoryStartPlace.z), 0.1f);
            //pull downwards
        }
    }
}
